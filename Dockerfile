FROM registry.gitlab.gnome.org/gnome/gnome-runtime-images/gnome:master
USER root

ADD flatpak-builder flatpak-builder
WORKDIR flatpak-builder
RUN dnf install -y attr fuse libubsan libasan libtsan elfutils-libelf-devel libdwarf-devel elfutils git gettext-devel libappstream-glib-devel bison libcurl-devel dconf-devel fuse-devel autoconf automake libtool glib-devel libsoup-devel ostree json-glib-devel libxml2-devel ostree-devel libxslt debugedit
RUN ./autogen.sh --with-system-debugedit=yes --enable-documentation=no
RUN make install
WORKDIR ..
RUN rm -rdf flatpak-builder

USER build
